# Kodi Media Center language file
# Addon Name: PCTV Systems Client
# Addon id: pvr.pctv
# Addon Provider: PCTV Systems
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2025-01-17 23:19+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Japanese <https://kodi.weblate.cloud/projects/kodi-add-ons-pvr-clients/pvr-pctv/ja_jp/>\n"
"Language: ja_jp\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.9.2\n"

msgctxt "Addon Summary"
msgid "PVR Client to connect PCTV Systems Broadway to Kodi"
msgstr "PCTV Systems BroadwayをKodiに接続するためのPVRクライアント"

msgctxt "Addon Description"
msgid "PCTV Systems frontend. Supporting streaming of Live TV & Recordings, EPG View, Timers."
msgstr "PCTVシステム ソフトウェア。ストリーミングに関するTV視聴と録画、EPG表示、タイマーをサポート。"

msgctxt "Addon Disclaimer"
msgid "This is unstable software! The authors are in no way responsible for failed playings, incorrect EPG times, wasted hours, or any other undesirable effects."
msgstr "これは不安定なソフトウェアです！本プログラムの作者は、再生の失敗、EPG 時刻のずれ、無駄にした時間、その他あらゆる好ましくない結果について責任を負わないものとします。"

msgctxt "#30000"
msgid "Hostname or IP address"
msgstr "ホスト名かIPアドレス"

msgctxt "#30001"
msgid "Port"
msgstr "ポート"

msgctxt "#30002"
msgid "PIN"
msgstr "PIN"

msgctxt "#30010"
msgid "Automatic Host Detection"
msgstr "自動ホスト検出"

msgctxt "#30020"
msgid "Transcode"
msgstr "トランスコード"

msgctxt "#30021"
msgid "PIN required"
msgstr "PINが必要です"

msgctxt "#30030"
msgid "Video Bitrate (Kbps)"
msgstr "ビデオビットレート (Kbps)"

msgctxt "#30100"
msgid "General"
msgstr "一般"

msgctxt "#30101"
msgid "400 Kbps"
msgstr ""

msgctxt "#30102"
msgid "650 Kbps"
msgstr ""

msgctxt "#30103"
msgid "900 Kbps"
msgstr ""

msgctxt "#30104"
msgid "1200 Kbps"
msgstr ""

msgctxt "#30104"
msgid "2200 Kbps"
msgstr ""

msgctxt "#30105"
msgid "4200 Kbps"
msgstr ""

msgctxt "#30106"
msgid "6500 Kbps"
msgstr ""

msgctxt "#30107"
msgid "8500 Kbps"
msgstr ""
